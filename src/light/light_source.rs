//! Het 'lamp' type, is een lichtbron met een positie.

use vec::XYZ;
use light::Light;

/// Het 'lamp' type, is een lichtbron met een positie.
pub struct LightSource {
    pub pos: XYZ,
    pub light: Light,
}

impl LightSource {
    /// Maakt een nieuwe lamp aan aan de hand van de gegeven positie en lichtbron.
    pub fn new(pos: XYZ, light: Light) -> LightSource {
        LightSource {
            pos: pos,
            light: light,
        }
    }
}
