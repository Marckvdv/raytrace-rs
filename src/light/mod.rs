//! De module voor de (CIE-)belichting.
pub mod light_source;
pub mod light_ray;

use consts;
use material::Material;
use cie::CIEColor;
use vec::VecFloat;

/// De lengte van de lichtgrafiek-lijsten
pub const EMITTANCE_VALUES: usize = 64;

/// De golflengte (in meter) van het licht dat hoor bij het begin van het menselijke zicht
pub const WAVELENGTH_BEGIN: VecFloat   = 390e-9;

/// De golflengte (in meter) van het licht dat hoor bij het einde van het menselijke zicht
pub const WAVELENGTH_END:   VecFloat   = 830e-9;

/// Het menselijke zicht in meter
pub const WAVELENGTH_RANGE: VecFloat   = WAVELENGTH_END - WAVELENGTH_BEGIN;

/// De daadwerkelijke lichtgrafiek
pub struct Light {
    pub emittance: [VecFloat; EMITTANCE_VALUES]
}

impl Light {
    /// Maakt een lichtbron aan, aan de hand van de gegeven temperatuur en de formules van planck.
    pub fn from_temperature(kelvin: VecFloat, brightness: VecFloat) -> Light {
        let mut emittance = [0.0; EMITTANCE_VALUES];
        let mut total_brightness_squared = 0.0;
        for (idx, val) in emittance.iter_mut().enumerate() {
            let part = idx as VecFloat / EMITTANCE_VALUES as VecFloat;
            let wavelength = WAVELENGTH_BEGIN + part * WAVELENGTH_RANGE;

            let nominator = consts::BB_1 * wavelength.powi(-5);
            let denominator = consts::E.powf(consts::BB_2 / (wavelength * kelvin)) - 1.0;

            *val = nominator / denominator;
            total_brightness_squared += *val * *val;
        }

        let total_brightness = total_brightness_squared.sqrt();

        for val in emittance.iter_mut() {
            *val = (*val / total_brightness) * brightness;
        }

        Light {
            emittance: emittance,
        }
    }

    /// Maakt een lichtbron aan die enkel een golflengte uitzend.
    pub fn from_wavelength(wavelength: VecFloat, brightness: VecFloat) -> Light {
        let mut emittance = [0.0; EMITTANCE_VALUES];
        let idx = ((wavelength - WAVELENGTH_BEGIN) / WAVELENGTH_RANGE * EMITTANCE_VALUES as VecFloat) as usize;
        emittance[idx] = brightness;
        Light {
            emittance: emittance,
        }
    }

    /// Maakt een lichtbron aan die alle golflenges uitzendt.
    pub fn from_brightness(brightness: VecFloat) -> Light {
        Light {
            emittance: [brightness; EMITTANCE_VALUES]
        }
    }

    /// Maakt een lichtbron aan die **geen** licht uitzendt.
    pub fn new() -> Light {
        Light {
            emittance: [0.0; EMITTANCE_VALUES]
        }
    }

    /// Telt een lichtbron bij deze lichbron op.
    pub fn add_light(&mut self, other: &Light, modifier: VecFloat) {
        for (self_val, other_val) in self.emittance.iter_mut().zip(other.emittance.iter()) {
            *self_val += *other_val * modifier;
        }
    }

    /// Maakt deze lichbron donkerder door de absorptie van materiaal toe te passen.
    pub fn apply_material(&mut self, material: &Material) {
        for (light_val, mat_val) in self.emittance.iter_mut().zip(material.reflectance.iter()) {
            *light_val *= *mat_val;
        }
    }

    /// Zet deze lichtbron om in een CIE-kleur.
    pub fn to_cie(&self) -> CIEColor {
        CIEColor::from_data(&self.emittance)
    }
}
