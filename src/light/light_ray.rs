//! Een ray waaraan ook een lichbron is gekoppeld.

use light::Light;
use ray::Ray;

/// Een ray waaraan ook een lichbron is gekoppeld.
pub struct LightRay {
    /// De lichtbron
    pub light: Light,

    /// De ray
    pub ray: Ray,
}

impl LightRay {
    pub fn new(light: Light, ray: Ray) -> LightRay {
        LightRay {
            light: light,
            ray: ray,
        }
    }
}
