//! Module voor het inladen van scene's doormiddel van JSON. Voor voorbeelden, zie het mapje
//! "/scenes". Een voorbeeld van een scene:
//!
//! ```
//!{
//!  "camera" : {
//!    "pos" : [0.00001, -2, 0],
//!    "dir" : [0, 1, 0],
//!    "res" : [400, 400],
//!    "fov" : 90,
//!    "lens" : "standard"
//!  },
//!
//!  "planes" : [
//!    { "pos": [0,0,-2], "normal": [0,0,1], "matr": "red" },
//!  ],
//!
//!  "spheres" : [
//!    { "pos": [0,1,1], "radius": 1, "matr": "red" },
//!  ],
//!
//!  "triangles" : [
//!     { "points" : [[-1, 0, 0], [1, 0, 0], [0, 1, 0]], "matr" : "blue" }
//!  ],
//!
//!  "models" : [
//!    { "pos": [0, 0.5, 0], "scale": 0.1, "matr": "dblue", "obj_file": "models/daft.obj" }
//!  ],
//!
//!  "bb_lights" : [ 
//!    { "pos": [0, -1, 0], "temperature": 6000, "brightness": 40 }
//!  ],
//!
//!  "eq_lights" : [
//!    { "pos": [0, -2, 0], "brightness": 40 }
//!  ],
//!
//!  "lights" : [
//!    { "pos": [0, 0, 0], "wavelength": 700, "brightness": 40 }
//!  ],
//!
//!  "materials": [
//!    { "name": "white", "reflectance": [0.99] },
//!    { "name": "grey", "reflectance": [0.4] },
//!    { "name": "red", "reflectance": [0.2, 0.2, 1, 0.2] },
//!    { "name": "blue", "reflectance": [1, 0.2, 0.2] },
//!    { "name": "dblue", "reflectance": [0.5, 0.1, 0.1] },
//!    { "name": "green", "reflectance": [0.2, 1, 0.2, 0.2] },
//!    { "name": "yellow", "reflectance": [0.2, 0.5, 1, 0.5, 0.2, 0.2] }
//!  ]
//!}
//! ```

use rustc_serialize::json;
use std::collections::HashMap;

use solid::plane::Plane;
use solid::sphere::Sphere;
use solid::triangle::Triangle;
use solid::model::Model;
use solid::RenderableSolid;
use light::Light;
use light::light_source::LightSource;
use camera::Camera;
use camera::standard_camera::StandardCamera;
use camera::fisheye_camera::FishEyeCamera;
use material::Material;
use vec::{XYZ, VecFloat};
use scene::Scene;

#[derive(RustcDecodable, Debug)]
struct CameraJSON {
    pos: [VecFloat; 3],
    dir: [VecFloat; 3],
    res: [VecFloat; 2],
    fov: VecFloat,
    lens: String,
}

impl CameraJSON {
    fn to_camera(&self) -> Box<Camera + Sync> {
        let camera: Box<Camera + Sync> = match &self.lens[..] {
            "standard" => {
                Box::new(StandardCamera::new(
                    XYZ::from_array(self.pos),
                    XYZ::from_array(self.dir).normalize(),
                    (self.res[0] as usize, self.res[1] as usize),
                    self.fov,
                ))
            },
            "fisheye" => {
                Box::new(FishEyeCamera::new(
                    XYZ::from_array(self.pos),
                    XYZ::from_array(self.dir).normalize(),
                    (self.res[0] as usize, self.res[1] as usize),
                    self.fov,
                ))
            },
            _ => {
                panic!("Unknown camera type");
            }
        };
        camera
    }
}

#[derive(RustcDecodable, Debug)]
struct PlaneJSON {
    pos: [VecFloat; 3],
    normal: [VecFloat; 3],
    matr: String,
}

impl PlaneJSON {
    fn to_plane(&self) -> Plane {
        Plane::new(
            XYZ::from_array(self.pos),
            XYZ::from_array(self.normal).normalize(),
        )
    }
}

#[derive(RustcDecodable, Debug)]
struct SphereJSON {
    pos: [VecFloat; 3],
    radius: VecFloat,
    matr: String,
}

impl SphereJSON {
    fn to_sphere(&self) -> Sphere {
        Sphere::new(
            XYZ::from_array(self.pos),
            self.radius,
        )
    }
}

#[derive(RustcDecodable, Debug)]
struct TriangleJSON {
    points: [[VecFloat; 3]; 3],
    matr: String,
}

impl TriangleJSON {
    fn to_triangle(&self) -> Triangle {
        Triangle::new(
            XYZ::from_array(self.points[0]),
            XYZ::from_array(self.points[1]),
            XYZ::from_array(self.points[2]),
        )
    }
}

#[derive(RustcDecodable, Debug)]
struct ModelJSON {
    obj_file: String,
    pos: [VecFloat; 3],
    scale: VecFloat,
    matr: String,
}

impl ModelJSON {
    fn to_models(&self) -> Vec<Model> {
        Model::open(
            &self.obj_file,
            XYZ::from_array(self.pos),
            self.scale,
        )
    }
}

#[derive(RustcDecodable, Debug)]
struct BBLightJSON {
    pos: [VecFloat; 3],
    temperature: VecFloat,
    brightness: VecFloat,
}

impl BBLightJSON {
    fn to_light(&self) -> LightSource {
        LightSource::new(XYZ::from_array(self.pos),
            Light::from_temperature(
                self.temperature,
                self.brightness,
            )
        )
    }
}

#[derive(RustcDecodable, Debug)]
struct LightJSON {
    pos: [VecFloat; 3],
    wavelength: VecFloat,
    brightness: VecFloat,
}

impl LightJSON {
    fn to_light(&self) -> LightSource {
        LightSource::new(XYZ::from_array(self.pos),
            Light::from_wavelength(
                self.wavelength * 1e-9,
                self.brightness,
            )
        )
    }
}

#[derive(RustcDecodable, Debug)]
struct EqLightJSON {
    pos: [VecFloat; 3],
    brightness: VecFloat,
}

impl EqLightJSON {
    fn to_light(&self) -> LightSource {
        LightSource::new(XYZ::from_array(self.pos),
            Light::from_brightness(
                self.brightness,
            )
        )
    }
}

#[derive(RustcDecodable, Debug)]
struct MaterialJSON {
    name: String,
    reflectance: Vec<VecFloat>,
}

impl MaterialJSON {
    pub fn to_material(&self) -> (String, Material) {
        (self.name.clone(), Material::from_values(&self.reflectance))
    }
}

#[derive(RustcDecodable, Debug)]
pub struct SceneJSON {
    camera: CameraJSON,
    planes: Vec<PlaneJSON>,
    spheres: Vec<SphereJSON>,
    triangles: Vec<TriangleJSON>,
    models: Vec<ModelJSON>,
    bb_lights: Vec<BBLightJSON>,
    lights: Vec<LightJSON>,
    eq_lights: Vec<EqLightJSON>,
    materials: Vec<MaterialJSON>
}

impl SceneJSON {
    pub fn from_string(string: &str) -> SceneJSON {
        json::decode(string).expect("Invalid json file")
    }

    pub fn to_scene(&self) -> Scene {
        let camera = self.camera.to_camera();
        let mut scene = Scene::new(camera);

        let mut materials = HashMap::new();
        for (name, material) in self.materials.iter().map(|v| v.to_material()) {
            materials.insert(name, material);
        }

        for plane_json in self.planes.iter() {
            let material = materials.get(&plane_json.matr).unwrap().clone();
            let plane = plane_json.to_plane();
            scene.solids.push(RenderableSolid::new(Box::new(plane), material.clone()));
        }

        for sphere_json in self.spheres.iter() {
            let material = materials.get(&sphere_json.matr).unwrap().clone();
            let sphere = sphere_json.to_sphere();
            scene.solids.push(RenderableSolid::new(Box::new(sphere), material.clone()));
        }

        for triangle_json in self.triangles.iter() {
            let material = materials.get(&triangle_json.matr).unwrap().clone();
            let triangle = triangle_json.to_triangle();
            scene.solids.push(RenderableSolid::new(Box::new(triangle), material.clone()));
        }

        for models_json in self.models.iter() {
            let material = materials.get(&models_json.matr).unwrap().clone();
            for model in models_json.to_models() {
                scene.solids.push(RenderableSolid::new(Box::new(model), material.clone()));
            }
        }

        for bb_light in self.bb_lights.iter().map(|v| v.to_light()) {
            scene.lights.push(bb_light);
        }

        for light in self.lights.iter().map(|v| v.to_light()) {
            scene.lights.push(light);
        }

        for eq_light in self.eq_lights.iter().map(|v| v.to_light()) {
            scene.lights.push(eq_light);
        }

        scene
    }
}
