//! Scene's die gerenderd kunnen worden. Voor het inladen van scene's, zie `scene_json`.
use std::path::Path;
use std::fs::File;
use std::io::Read;

use camera::Camera;
use solid::RenderableSolid;
use light::light_source::LightSource;

pub mod scene_json;

/// De informatie van een bepaalde scene
pub struct Scene {
    /// De camera die voor deze scene gebruikt wordt.
    pub camera: Box<Camera + Sync>,

    /// De voorwerpen die gerenderd moet worden.
    pub solids: Vec<RenderableSolid>,

    /// De lichten die in de scene staan.
    pub lights: Vec<LightSource>,
}

impl Scene {
    /// Maakt een lege scene aan de hand van een camera.
    pub fn new(camera: Box<Camera + Sync>) -> Scene {
        Scene {
            camera: camera,
            solids: Vec::new(),
            lights: Vec::new(),
        }
    }

    /// Laadt een scene in vanuit JSON.
    pub fn load_from_json(path: &Path) -> Scene {
        let file_data = {
            let mut buf = String::new();
            File::open(path).unwrap().read_to_string(&mut buf).unwrap();
            buf
        };

        let scene_json = scene_json::SceneJSON::from_string(&file_data);
        scene_json.to_scene()
    }
}
