//! De module voor het exporteren van de plaatjes. Bevat een functie die een lijst van pixels om
//! kan zetten naar een (BMP-)plaatje.
use pixel;
use bmp::{Image, Pixel};

/// Zet een lijst van pixels om in een BMP-plaatje.
pub fn export_buffer(pixels: Vec<pixel::Pixel>, res: (usize, usize), path: &str) {
    let mut image = Image::new(res.0 as u32, res.1 as u32);
    for (i, (x, y)) in image.coordinates().enumerate() {
        let pixel = pixels[i];
        let to_store = Pixel{ r: pixel.r, g: pixel.g, b: pixel.b };
        image.set_pixel(x, y, to_store);
    }

    image.save(path).expect("Unable to save the image");
}
