//! De camera's
use vec::XYZ;

/// De standaard camera
pub mod standard_camera;

/// Een camera met een fisheye lens
pub mod fisheye_camera;

/// Alle methodes die een camera moet implementeren.
pub trait Camera {
    /// Geeft de directie van de ray die door de aangegeven pixel heen gaat.
    fn get_dir(&self, pixel: (usize, usize)) -> XYZ;

    /// Geeft de resolutie van het scherm.
    fn get_res(&self) -> (usize, usize);

    /// Geeft de positie van de camera in de scene.
    fn get_pos(&self) -> XYZ;
}
