use camera::Camera;
use vec::{XYZ, VecFloat};
use consts;

/// De fisheye camera.  Een fisheye camera is een camera met een fisheye lens.
pub struct FishEyeCamera {
    /// Positie van de camera
    pub pos: XYZ,               

    /// Directie van de camera
    pub dir: XYZ,               

    /// Breedte en hoogte van het scherm
    pub res: (usize, usize),    

    /// Horizontale en verticale kijkhoek (fov = Field Of View)
    pub fov: (VecFloat, VecFloat), 

    /// Breedte die het scherm inneemt in de scene
    pub screen_width: VecFloat,

    /// Hoogte die het scherm inneemt in de scene
    pub screen_height: VecFloat,
}

impl Camera for FishEyeCamera {
    /// Returnt de resolutie van het scherm.
    fn get_res(&self) -> (usize, usize) {
        self.res
    }
    
    /// Returnt de positie van de camera in de scene.
    fn get_pos(&self) -> XYZ {
        self.pos
    }

    /// Returnt de directie van de ray die door de aangegeven pixel heen gaat. Dit gebeurt door de
    /// de pixel positie om te rekenen naar een hoek. Dit gebeurt door de pixel positie om te
    /// rekenen naar een percentage en deze  vervolgens keer de kijkhoek te doen.
    fn get_dir(&self, pixel: (usize, usize)) -> XYZ {
        let (x_percentage, y_percentage) =
            (pixel.0 as VecFloat / self.res.0 as VecFloat,
             pixel.1 as VecFloat / self.res.1 as VecFloat);
        let (h_angle, v_angle) = 
            ((x_percentage - 0.5) * self.fov.0,
             (y_percentage - 0.5) * self.fov.1);

        XYZ::new(
            v_angle.cos() * h_angle.sin(),
            v_angle.cos() * h_angle.cos(),
            v_angle.sin(),
        )
    }
}

impl FishEyeCamera {
    /// Maakt een nieuwe standaard camera aan op basis van een positie, directie, resolutie en
    /// kijkhoek.
    pub fn new(pos: XYZ, dir: XYZ, res: (usize, usize), fov: VecFloat) -> FishEyeCamera {
        let aspect_ratio = res.1 as VecFloat / res.0 as VecFloat;
        let v_fov = fov * aspect_ratio;

        let (fov, v_fov) = (fov / 180.0 * consts::PI, v_fov / 180.0 * consts::PI); 
        let (screen_width, screen_height) = ((fov/2.0).tan()*2.0, (v_fov/2.0).tan()*2.0);
        println!("Screen_width: {}, screen_height: {}, fov: {:?}", screen_width, screen_height, (fov, v_fov));

        FishEyeCamera {
            pos: pos,
            dir: dir,
            res: res,
            fov: (fov, v_fov),
            screen_width: screen_width,
            screen_height: screen_height,
        }
    }

}
