//! De standaard camera, zoals een normale camera in principe ook werkt.
use vec::{XYZ, VecFloat};
use camera::Camera;
use consts;

/// De standaard camera, zoals een normale camera in principe ook werkt.
pub struct StandardCamera {
    /// Positie van de camera
    pub pos: XYZ,               
        /// Directie van de camera
        pub dir: XYZ,               

        /// Breedte en hoogte van het scherm
        pub res: (usize, usize),    

        /// Horizontale en verticale kijkhoek (fov = Field Of View)
        pub fov: (VecFloat, VecFloat),        

        /// De vector die naar rechts ten opzichte van de camera wijst (alleen voor intern gebruik)
        right: XYZ,                 

        /// De vector die naar omhoog ten opzichte van de camera wijst (alleen voor intern gebruik)
        up: XYZ,                    

        /// De breedte die het scherm inneemt in de scene
        pub screen_width: VecFloat,      

        /// De hoogte die het scherm inneemt in de scene
        pub screen_height: VecFloat,     
}

impl Camera for StandardCamera {
    /// Returnt de resolutie van het scherm.
    fn get_res(&self) -> (usize, usize) {
        self.res
    }

    /// Returnt de positie van de camera in de scene.
    fn get_pos(&self) -> XYZ {
        self.pos
    }

    /// Returnt de directie van de ray die door de aangegeven pixel heen gaat. Deze wordt berekent
    /// door als eerst uit te rekenen op welk deel de pixel op het scherm zit (percentage). Hierna
    /// worden deze delen keer de right en up vector gedaan en bij de camera directie opgeteld (en
    /// genormaliserd).
    fn get_dir(&self, pixel: (usize, usize)) -> XYZ {
        let (p_x, p_y) = 
            (pixel.0 as VecFloat / self.res.0 as VecFloat,
             pixel.1 as VecFloat / self.res.1 as VecFloat);
        let (p_x, p_y) = (p_x - 0.5, p_y - 0.5);

        let to_add = 
            self.up    * p_y * self.screen_height +
            self.right * p_x * self.screen_width;
        (self.dir + to_add).normalize()
    }
}

impl StandardCamera {
    /// Maakt een nieuwe standaard camera aan op basis van een positie, directie, resolutie en
    /// kijkhoek.
    pub fn new(pos: XYZ, dir: XYZ, res: (usize, usize), fov: VecFloat) -> StandardCamera {
        // Bepaalt de right en up vector
        let (right, up) = StandardCamera::get_right_and_up(dir);    

        // Berekent de aspect ratio die bij de resolutie hoort
        let aspect_ratio = res.1 as VecFloat / res.0 as VecFloat;             

        // Rekent de kijkhoek om naar radialen, zodat deze gebruikt kan worden in de goniometrische
        // functies.
        let fov = fov / 180.0 * consts::PI;

        // Berekent de verticale kijkhoek aan de hand van de horizontale kijkhoek en de aspectratio
        let v_fov = 2.0 * ((fov / 2.0).tan() * aspect_ratio).atan();

        // Berkent de grootte die het scherm inneemt in de scene
        let (screen_width, screen_height) = ((fov/2.0).tan()*2.0, (v_fov/2.0).tan()*2.0);

        StandardCamera {
            pos: pos,
            dir: dir,
            res: res,
            fov: (fov, v_fov),
            right: right,
            up: up,
            screen_width: screen_width,
            screen_height: screen_height,
        }
    }


    /// Berekent de right en up vector die benodigd is om de rays te bepalen.
    /// De right vector wordt berekent door het uitwendige product te nemen tussen de 'omhoog'
    /// vector en de directie van de camera. Hierna wordt de up vector berekent uit het uitwendige
    /// product tussen de right vector en de directie van de camera.
    fn get_right_and_up(dir: XYZ) -> (XYZ, XYZ) {
        let right = dir.cross(XYZ::new(0.0, 0.0, 1.0));
        let up = right.cross(dir);

        (right, up)
    }
}
