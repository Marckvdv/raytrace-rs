//! Materialen die gebruikt kunnen worden.
use light::{EMITTANCE_VALUES};
use vec::VecFloat;

/// Type dat het materiaal aangeeft van een voorwerp. TODO op het moment geeft het materiaal alleen
/// aan hoe goed het materiaal een bepaalde golflengte reflecteerd (oftewel, welke kleur het is).
pub struct Material {
    /// Alle reflectie waarden
    pub reflectance: [VecFloat; EMITTANCE_VALUES],
}

impl Material {
    /// Maakt een nieuw materiaal aan, aan de hand van de gegeven waardes.
    pub fn new(reflectance: [VecFloat; EMITTANCE_VALUES]) -> Material {
        Material {
            reflectance: reflectance
        }
    }
    
    /// Maakt een nieuw materiaal aan, aan de hand van de gegeven waardes. Interpoleert wanneer er
    /// minder waardes zijn gegeven dan dat er nodig zijn.
    pub fn from_values(values: &[VecFloat]) -> Material {
        let mut reflectance = [0.0; EMITTANCE_VALUES];

        for (index, val) in reflectance.iter_mut().enumerate() {
            let percentage = (index as VecFloat) / (EMITTANCE_VALUES as VecFloat);
            *val = values[(percentage * values.len() as VecFloat) as usize];
        }

        Material::new(reflectance)
    }
}

impl Clone for Material {
    fn clone(&self) -> Material {
        Material {
            reflectance: self.reflectance
        }
    }
}
