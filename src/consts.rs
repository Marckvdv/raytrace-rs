//! Alle constantes die in de raytracer gebruikt worden

use vec::VecFloat;

#[allow(unused_imports)]
use std::{f32, f64};

/// De constante pi
pub const PI    : VecFloat = f64::consts::PI as VecFloat;

/// De constante 2 * pi
pub const PI_2  : VecFloat = PI * 2.0;

/// De lichtsnelheid in vacuüm
pub const C     : VecFloat = 2.99792458e8;

/// De constante van Planck
pub const PLANCK: VecFloat = 6.62607004e-34;

/// De constante van Boltzman
pub const K     : VecFloat = 1.380658e-23;

/// De euler e constante
pub const E     : VecFloat = 2.71828182846;

/// De eerste constante die nodig is voor het generenen van Planck-krommes (= 2 * pi * p * c^2)
pub const BB_1  : VecFloat = PI_2 * PLANCK * C * C;

/// De tweede constante die nodig is voor het generenen van Planck-krommes (= p * c / k)
pub const BB_2  : VecFloat = PLANCK * C / K;

/// Oneindig (grootste 'getal' mogelijk in een float)
pub const INFINITY: VecFloat = 1.0/0.0;
