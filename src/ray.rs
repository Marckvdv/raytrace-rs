//! Het ray type, bevat informatie over de positie en de directie van de ray.
use vec::XYZ;

/// Het ray type, bevat informatie over de positie en de directie van de ray.
#[derive(Clone, Copy)]
pub struct Ray {
    /// Het beginpunt van de ray
    pub pos: XYZ,

    /// De directievector van de ray
    pub dir: XYZ
}

impl Ray {
    /// Maakt een nieuwe ray aan de hand van de gegeven positie en directie.
    pub fn new(pos: XYZ, dir: XYZ) -> Ray {
        Ray {
            pos: pos,
            dir: dir,
        }
    }
}
