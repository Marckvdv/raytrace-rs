//! Alle voorwerpen die gerenderd kunnen worden.
use ray::Ray;
use hit::Hit;
use material::Material;

/// Bol
pub mod sphere;

/// Vlak
pub mod plane;

/// Driehoek
pub mod triangle;

/// Model
pub mod model;

/// Trait die aanwezig moet zijn om gerenderd te kunnen worden.
pub trait Solid {
    /// Functie die bepaalt of een ray het voorwerp heeft geraakt.
    fn intersect(&self, ray: &Ray) -> Option<Hit>;
}

pub struct RenderableSolid {
    pub solid: Box<Solid + Sync>,
    pub material: Material,
}

impl RenderableSolid {
    pub fn new(solid: Box<Solid + Sync>, material: Material) -> RenderableSolid {
        RenderableSolid {
            solid: solid,
            material: material,
        }
    }
}
