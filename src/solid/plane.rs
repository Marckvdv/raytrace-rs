//! Module voor het maken en renderen van vlakken.

use vec::XYZ;
use solid::Solid;
use ray::Ray;
use hit::Hit;

/// De structuur van een vlak
pub struct Plane {
    pos: XYZ,
    normal: XYZ,
}

impl Plane {
    /// Maakt een nieuw vlak aan met de gegeven positie en normaal.
    pub fn new(pos: XYZ, normal: XYZ) -> Plane {
        Plane {
            pos: pos,
            normal: normal,
        }
    }
}

impl Solid for Plane {
    fn intersect(&self, ray: &Ray) -> Option<Hit> {
        // Onderkant van de breuk (R . N)
        let denominator = ray.dir.dot(self.normal);
        
        // Dit mag geen 0 zijn (delen door 0)
        if denominator != 0.0 {
            // Stel D = V - P
            let d = self.pos - ray.pos;

            // Bovenkant van de breuk (D . N)
            let numerator = d.dot(self.normal);

            let t = numerator / denominator;
            if t < 0.0 { return None; }
            let hit_pos = ray.pos + ray.dir * t;

            Some(Hit::new(hit_pos, self.normal))
        } else {
            // Als deze dus wel 0 is, is er geen oplossing
            // (Het vlak is dan parallel aan de ray)
            None
        }
    }
}
