//! Module voor het maken en renderen van driehoeken.
use vec::{XYZ, VecFloat};
use solid::Solid;
use ray::Ray;
use hit::Hit;

/// De structuur van een driehoek
pub struct Triangle {
    // Zelfde namen voor de hoekpunten als in het verslag
    pub a: XYZ,
    pub b: XYZ,
    pub c: XYZ,

    pub perp_ab: XYZ,
    pub perp_bc: XYZ,
    pub perp_ca: XYZ,

    pub n: XYZ,
}

impl Triangle {
    /// Maakt een driehoek aan met de gegeven hoekpunten.
    pub fn new(a: XYZ, b: XYZ, c: XYZ) -> Triangle {
        let normal = ((c - a).cross(b - a)).normalize();
        Triangle {
            a: a,
            b: b,
            c: c,

            perp_ab: normal.cross(b - a),
            perp_bc: normal.cross(c - b),
            perp_ca: normal.cross(a - c),

            n: normal,
        }
    }

    /// Maakt een driehoek aan met de gegeven hoekpunten, maar gebruikt de gegeven normaal
    /// (in tegenstelling tot `new`), in plaats van ze zelf te berekenen.
    pub fn with_normal(a: XYZ, b: XYZ, c: XYZ, normal: XYZ) -> Triangle {
        Triangle {
            a: a,
            b: b,
            c: c,

            perp_ab: normal.cross(b - a),
            perp_bc: normal.cross(c - b),
            perp_ca: normal.cross(a - c),

            n: normal,
        }
    }
}

impl Solid for Triangle {
    fn intersect(&self, ray: &Ray) -> Option<Hit> {
        let d = self.a - ray.pos;

        let denom = ray.dir.dot(self.n);
        if denom != 0.0 {
            let t = (d.dot(self.n)) / denom;
            if t < 0.0 { return None; }

            let x = ray.pos + ray.dir * t;

            fn inside(dot_a: u32, dot_b: u32, dot_c: u32) -> bool {
                dot_a == dot_b && dot_b == dot_c
            }

            fn signum(f: VecFloat) -> u32 {
                if f > 0.0 {
                    1
                } else {
                    2
                }
            }

            let (a, b, c) = (
                (x - self.a).dot(self.perp_ab),
                (x - self.b).dot(self.perp_bc),
                (x - self.c).dot(self.perp_ca),
            );

            if inside(
                signum(a),
                signum(b),
                signum(c)) {

                Some(Hit::new(x, self.n))
            } else {
                None
            }
        } else {
            None
        }
    }
}
