//! Module voor het maken en renderen van bollen.
use vec::{XYZ, VecFloat};
use solid::Solid;
use ray::Ray;
use hit::Hit;

/// De structuur van een bol.
pub struct Sphere {
    pos: XYZ,
    radius: VecFloat,
}

impl Sphere {
    /// Maakt een nieuwe bol aan met de gegeven positie en straal.
    pub fn new(pos: XYZ, radius: VecFloat) -> Sphere {
        Sphere {
            pos: pos,
            radius: radius,
        }
    }
}

impl Solid for Sphere {
    fn intersect(&self, ray: &Ray) -> Option<Hit> {
        // Stel D = P - B
        #![allow(non_snake_case)]
        let D = ray.pos - self.pos;

        let a = ray.dir.dot(ray.dir);                       // a = R . R
        let b = 2.0 * D.dot(ray.dir);                       // b = 2(R . D)
        let c = D.dot(D) - (self.radius * self.radius);     // c = D . D - r^2

        let d = b * b - 4.0 * a * c;                        // d = b^2 - 4ac
        if d >= 0.0 {
            let denominator = 2.0 * a;

            // ABC-formule
            let t1 = (-b + d.sqrt())/denominator;
            let t2 = (-b - d.sqrt())/denominator;

            let closest = t1.min(t2);
            if closest < 0.0 { return None; }

            let hit_pos = ray.pos + ray.dir * closest;
            
            Some(Hit::new(hit_pos, (hit_pos - self.pos) / self.radius))
        } else { None }
    }
}
