//! Module voor het inladen, opslaan en renderen van modellen

use solid::Solid;
use solid::triangle::Triangle;
use vec::{XYZ, VecFloat};
use hit::Hit;
use ray::Ray;
use consts;

use wavefront_obj::obj::{Object, Vertex, Shape};
use wavefront_obj::obj;

use std::path::Path;
use std::fs::File;
use std::io::Read;
use std::mem;

/// Structuur voor het opslaan van de modellen
pub struct Model {
    triangles: Vec<Triangle>,
}

impl Model {
    /// Maakt een nieuw model aan, aan de hand van de gegeven driehoeken.
    fn new(triangles: Vec<Triangle>) -> Model {
        Model {
            triangles: triangles,
        }
    }

    /// Zet een object om in een lijst van driehoeken en past de gegeven translatie en schaling toe.
    fn object_to_triangles(object: &Object, translation: XYZ, scale: VecFloat) -> Vec<Triangle> {
        let mut triangles = Vec::new();

        fn convert_vertex(vertex: Vertex) -> XYZ {
            XYZ::new(vertex.x as VecFloat, vertex.y as VecFloat, vertex.z as VecFloat)
        }

        for geometry in object.geometry.iter() {
            for &shape in geometry.shapes.iter() {
                if let Shape::Triangle(a_v, b_v, c_v) = shape {
                    let (a, b, c) = (
                        object.vertices[a_v.0],
                        object.vertices[b_v.0],
                        object.vertices[c_v.0],
                    );

                    let (a, b, c) = (
                        convert_vertex(a) * scale + translation,
                        convert_vertex(b) * scale + translation,
                        convert_vertex(c) * scale + translation,
                    );

                    let triangle = match a_v.2 {
                        Some(normal_index) => {
                            let normal = convert_vertex(object.normals[normal_index]);
                            Triangle::with_normal(a, b, c, normal)
                        },
                        None => Triangle::new(a, b, c)
                    };

                    triangles.push(triangle);
                }
            }
        }
        triangles
    }

    /// Opent het pad en zet het om in een lijst van modellen.
    pub fn open(path: &str, translation: XYZ, scale: VecFloat) -> Vec<Model> {
        if let Ok(mut file) = File::open(Path::new(path)) {
            let mut content = String::new();
            file.read_to_string(&mut content)
                .ok().expect("Failed to read to string while loading model");

            let obj_set = obj::parse(content).unwrap();

            let mut models = Vec::new();
            for obj in obj_set.objects.iter() {
                let triangles = Model::object_to_triangles(&obj, translation, scale);
                println!("Loaded: {}, {} triangles!", path, triangles.len());

                models.push(Model::new(triangles));
            }
            models
        } else {
            panic!("Could not open file!");
        }
    }
}

impl Solid for Model {
    fn intersect(&self, ray: &Ray) -> Option<Hit> {
        let mut closest_dist: VecFloat = consts::INFINITY;
        let mut closest: Hit = unsafe { mem::uninitialized() };
        let mut hit = false;

        for triangle in self.triangles.iter() {
            match triangle.intersect(&ray) {
                Some(ref hit_info) => {
                    let dist = hit_info.pos.dist(ray.pos);
                    if dist > 0.0 && dist < closest_dist { 
                        closest_dist = dist;
                        closest = *hit_info;
                        hit = true;
                    }
                },
                _ => (),
            }
        }

        if hit {
            Some(closest)
        } else {
            None
        }

    }
}
