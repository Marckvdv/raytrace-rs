//! De implementatie van de CIE-kleurruimte.
//! Mijn implementatie kan een willekeurige golflengte/intensiteit-grafiek omzetten in een
//! CIE-kleur, die vervolgens weer kan worden omgezet in een RGB-kleur (zie `CIEColor.to_rgb`).

// Implemented using: 'http://www.fourmilab.ch/documents/specrend/specrend.c' as a reference

use pixel::Pixel;
use vec::VecFloat;
use cgmath::{Matrix3, Vector3};
use light::EMITTANCE_VALUES;

/// Het CIE-kleur type, dit wordt opgeslagen als CIE-xyY zodat helderheid ook wordt meegenomen.
pub struct CIEColor {
    /// Het x-component (= genormaliseerde *X*-component)
    pub x: VecFloat,

    /// Het y-component (= genormaliseerde *Y*-component)
    pub y: VecFloat,

    /// Het Y-component. Deze geeft dus de helderheid aan van de kleur.
    pub Y: VecFloat,
}

impl CIEColor {
    /// Zet een golflengte/intensiteit-grafiek om in een CIE-kleur.
    pub fn from_data(data: &[VecFloat]) -> CIEColor {
        let (mut total_x, mut total_y, mut total_z) = (0.0, 0.0, 0.0);

        for (index, intensity) in data.iter().enumerate() {
            let matcher_index =
                ((index as VecFloat / data.len() as VecFloat) * MATCHER_VALUES.len() as VecFloat) as usize;

            total_x += intensity * MATCHER_VALUES[matcher_index][0];
            total_y += intensity * MATCHER_VALUES[matcher_index][1];
            total_z += intensity * MATCHER_VALUES[matcher_index][2];
        }

        let total = total_x + total_y + total_z;

        CIEColor {
            x: total_x / total,
            y: total_y / total,
            Y: total_y / (EMITTANCE_VALUES as VecFloat)
        }
    }

    pub fn to_rgb(&self) -> Pixel {
        let conv_matrix = Matrix3::<VecFloat>::new(
            0.41847, -0.091169, 0.00092090,
            -0.15866, 0.25243, -0.0025498,
            -0.082835, 0.015708, 0.17860,
        );
        let color_vec = Vector3::new(
            (self.Y/self.y)*self.x, 
            self.Y, 
            (self.Y/self.y)*(1.0 - self.x - self.y)
        );   // Convert xyY to XYZ

        let color: Vector3<VecFloat> = conv_matrix * &color_vec;

        let (mut r, mut g, mut b) = (color.x, color.y, color.z);
        CIEColor::gamma_correct(&mut r, &mut g, &mut b, 2.2);
        CIEColor::constrain_values(&mut r, &mut g, &mut b);

        Pixel::from_floats(r, g, b)
    }

    fn gamma_correct(red: &mut VecFloat, green: &mut VecFloat, blue: &mut VecFloat, factor: VecFloat) {
        *red = red.powf(factor);
        *blue = blue.powf(factor);
        *green = green.powf(factor);
    }

    fn constrain_values(red: &mut VecFloat, green: &mut VecFloat, blue: &mut VecFloat) {
        let w = (0.0 as VecFloat).min(red.min(green.min(*blue))) * -1.0;

        if w > 0.0 { *red += w; *green += w; *blue += w; }

        if *red > 1.0 || *green > 1.0 || *blue > 1.0 {
            let greatest = red.max(green.max(*blue));
            *red /= greatest;
            *green /= greatest;
            *blue /= greatest;
        }
    }
}

/// De data die gebruikt wordt voor de 'color matching functions'. De data staat opgeslagen in
/// 'data/cie.csv' en kan naar eigen wensen worden aangepast.
pub const MATCHER_VALUES: [[VecFloat; 3]; 4401] = include!("../data/cie1.csv");
