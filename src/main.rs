//! Een raytracer geschreven in de programmeertaal Rust door Marck van der Vegt. Deze raytracer heb
//! ik gemaakt voor mijn profielwerstuk, die over dit onderwerp gaat. Ik heb dit gedaan in de 5e en
//! 6e klas (2015 - 2016) aan de TCC-Lyceumstraat. De laatste render van mijn raytracer:
//!
//! Gebruik maken van mijn raytracer gaat als volgt:
//!
//! ```sh
//! $ cargo run --release SCENE_BESTAND
//! ```
//!
//! <img alt="Voorbeeld render" src="../../../images/daft.png" style="width: 100%"/>

extern crate raytracer;
extern crate clock_ticks;

use raytracer as ray;
use raytracer::scene::Scene;
use raytracer::camera::Camera;

use std::path::Path;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 { panic!("Not enough arguments supplied!"); }

    let scene_path = Path::new(args.iter().nth(1).unwrap());
    let scene = Scene::load_from_json(&scene_path);

    let start = clock_ticks::precise_time_ns();
    let pixels = ray::raytracer::render(&scene, &ray::raytracer::lighting::handle_hit, 8);
    let end = clock_ticks::precise_time_ns();

    let took = end - start;
    println!("Rendering took: {} seconds", (took as f32) / 1_000_000_000.0);

    ray::export::export_buffer(pixels, scene.camera.get_res(), &args.iter().nth(2).unwrap_or(&"out.bmp".to_string()));
}
