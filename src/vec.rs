//! Vector/positie type (XYZ)
use std::ops::{Sub, Add, Div, Mul};

/// Dit is het type float dat door het hele project heen gebruikt wordt. Wanneer een preciezer type
/// gebruikt wordt, is het renderen ook preciezer, maar duurt dit ook langer.
pub type VecFloat = f64;

#[derive(Debug, Clone, Copy, PartialEq, Default)]
/// Het vector type met componenten `x`, `y` en `z`.
pub struct XYZ {
    /// Het x-coordinaat
    pub x: VecFloat,

    /// Het y-coordinaat
    pub y: VecFloat,

    /// Het z-coordinaat
    pub z: VecFloat
}

impl XYZ {
    /// Maakt een nieuwe vector aan de hand van de drie componenten
    #[inline]
    pub fn new(x: VecFloat, y: VecFloat, z: VecFloat) -> XYZ {
        XYZ {
            x: x, 
            y: y, 
            z: z,
        }
    }

    /// Maakt een nieuwe vector aan aan de hand van de drie componenten in lijst vorm
    #[inline]
    pub fn from_array(array: [VecFloat; 3]) -> XYZ {
        XYZ::new(
            array[0],
            array[1],
            array[2],
        )
    }

    /// Berekent de afstand tussen dit punt en het andere punt.
    /// Dit gebeurt door de twee punten/vectoren van elkaar af te trekken en hiervan de lengte te
    /// bepalen.
    pub fn dist(&self, pos: XYZ) -> VecFloat {
        (pos - *self).length()
    }
}

/// Voor het optellen van 2 vectoren.
impl Add<XYZ> for XYZ {
    type Output = XYZ;

    #[inline]
    fn add(self, other: XYZ) -> XYZ {
        XYZ::new(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z
        )
    }
}

/// Voor het optellen van een vector en een decimaal getal.
impl Add<VecFloat> for XYZ {
    type Output = XYZ;

    #[inline]
    fn add(self, other: VecFloat) -> XYZ {
        XYZ::new(
            self.x + other,
            self.y + other,
            self.z + other,
        )
    }
}

/// Voor het aftrekken van 2 vectoren.
impl Sub<XYZ> for XYZ {
    type Output = XYZ;

    #[inline]
    fn sub(self, other: XYZ) -> XYZ {
        XYZ::new(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z
        )
    }
}

/// Voor het aftrekken van een vector en een decimaal getal.
impl Sub<VecFloat> for XYZ {
    type Output = XYZ;

    #[inline]
    fn sub(self, other: VecFloat) -> XYZ {
        XYZ::new(
            self.x - other,
            self.y - other,
            self.z - other,
        )
    }
}

/// Voor het delen van een vector door een decimaal getal.
impl Div<VecFloat> for XYZ {
    type Output = XYZ;

    #[inline]
    fn div(self, other: VecFloat) -> XYZ {
        XYZ::new(
            self.x / other,
            self.y / other,
            self.z / other,
        )
    }
}

/// Voor het delen van een vector door een andere vector
impl Div<XYZ> for XYZ {
    type Output = XYZ;

    #[inline]
    fn div(self, other: XYZ) -> XYZ {
        XYZ::new(
            self.x / other.x,
            self.y / other.y,
            self.z / other.z,
        )
    }
}

/// Voor het vermenigvuldigen van een vector met een decimaal getal.
impl Mul<VecFloat> for XYZ {
    type Output = XYZ;

    #[inline]
    fn mul(self, other: VecFloat) -> XYZ {
        XYZ::new(
            self.x * other,
            self.y * other,
            self.z * other,
        )
    }
}

/// Voor het vermenigvuldigen van een vector met een andere vector
impl Mul<XYZ> for XYZ {
    type Output = XYZ;

    #[inline]
    fn mul(self, other: XYZ) -> XYZ {
        XYZ::new(
            self.x * other.x,
            self.y * other.y,
            self.z * other.z,
        )
    }
}

impl XYZ {
    #[inline]
    pub fn length(self) -> VecFloat {
        self.dot(self).sqrt()
    }

    #[inline]
    pub fn dot(self, other: XYZ) -> VecFloat {
        self.x * other.x + 
        self.y * other.y + 
        self.z * other.z
    }

    #[inline]
    pub fn normalize(self) -> XYZ {
        self / self.length()
    }

    #[inline]
    pub fn cross(self, other: XYZ) -> XYZ {
        XYZ::new(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x,
        )
    }
}

#[test]
fn test_add() {
    let mut pos = XYZ::new(1.0, 2.0, 3.0);
    assert_eq!(pos, XYZ { x: 1.0, y: 2.0, z: 3.0 });

    pos = pos + XYZ::new(1.0, 2.0, 3.0);
    assert_eq!(pos, XYZ { x: 2.0, y: 4.0, z: 6.0 });
    pos = pos + 1.0;
    assert_eq!(pos, XYZ { x: 3.0, y: 5.0, z: 7.0 });
}

#[test]
fn test_sub() {
    let mut pos = XYZ::new(1.0, 2.0, 3.0);
    assert_eq!(pos, XYZ { x: 1.0, y: 2.0, z: 3.0 });

    pos = pos - XYZ::new(1.0, 2.0, 3.0);
    assert_eq!(pos, XYZ { x: 0.0, y: 0.0, z: 0.0 });
    pos = pos - 1.0;
    assert_eq!(pos, XYZ { x: -1.0, y: -1.0, z: -1.0 });
}

#[test]
fn test_div() {
    let mut pos = XYZ::new(8.0, 12.0, 4.0);
    pos = pos / 2.0;
    assert_eq!(pos, XYZ { x: 4.0, y: 6.0, z: 2.0 });
    pos = pos / 2.0;
    assert_eq!(pos, XYZ { x: 2.0, y: 3.0, z: 1.0 });
    pos = XYZ::new(8.0, 12.0, 4.0);
    pos = pos / XYZ::new(2.0, 3.0, 4.0);
    assert_eq!(pos, XYZ { x: 4.0, y: 4.0, z: 1.0 });
}

#[test]
fn test_mul() {
    let mut pos = XYZ::new(1.0, 2.0, 3.0);
    pos = pos * XYZ::new(3.0, 2.0, 1.0);
    assert_eq!(pos, XYZ { x: 3.0, y: 4.0, z: 3.0});
    pos = pos * 3.0;
    assert_eq!(pos, XYZ { x: 9.0, y: 12.0, z: 9.0});
}

#[test]
fn test_length() {
    let pos = XYZ::new(1.0, 1.0, 1.0);
    assert_eq!(pos.length(), 3.0f32.sqrt());
    let pos = XYZ::new(2.0, 3.0, 5.0);
    assert_eq!(pos.length(), 38.0f32.sqrt());
}

#[test]
fn test_dot() {
    let (pos1, pos2) = (XYZ::new(1.0, 2.0, 3.0), XYZ::new(3.0, 2.0, 1.0));
    assert_eq!(pos1.dot(pos2), 10.0);
}

#[test]
fn test_normalize() {
    let pos = XYZ::new(2.0, 3.0, 5.0).normalize();
    let len = pos.length();
    assert_eq!(len, 1.0);
}
