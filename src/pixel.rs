//! Pixel type dat gebruikt wordt om de plaatjes in op te slaan.
use vec::{XYZ, VecFloat};

/// Een pixel, bestaande uit de compontenten `r` (rood), `g` (groen), `b` (blauw). Voor elk kleur
/// component worden 8 bits gebruikt. In totaal zijn het dus 24 bits, wat genoeg is voor ongeveerd
/// 16 miljoen kleuren. Dit type wordt alleen gebruikt voor het opslaan van het plaatje, en is dus
/// omgezet van een CIE-kleur (zie `CIEColor.to_rgb()`).
#[derive(Clone, Copy)]
pub struct Pixel {
    /// Een byte voor rood
    pub r: u8,

    /// Een byte voor groen
    pub g: u8,

    /// Een byte voor blauw
    pub b: u8,
}

impl Pixel {
    /// Maakt een nieuwe pixel aan de hand van de meegegeven kleurcomponenten
    pub fn new(r: u8, g: u8, b: u8) -> Pixel {
        Pixel {
            r: r,
            g: g,
            b: b,
        }
    }

    /// Maakt een nieuwe pixel aan de hand van de meegegeven kleurcomponenten. Bij deze functie
    /// liggen de meegegeven floats tussen de 0 en 1, waarbij 0 staat voor bijvoorbeeld 0% rood, en
    /// 1 staat voor 100% rood.
    pub fn from_floats(r: VecFloat, g: VecFloat, b: VecFloat) -> Pixel {
        Pixel {
            r: (r * 255.0) as u8,
            g: (g * 255.0) as u8,
            b: (b * 255.0) as u8,
        }
    }

    /// Zelfde als de `from_floats` functie maar dan voor vectoren.
    pub fn from_vector(vec: XYZ) -> Pixel {
        Pixel {
            r: (vec.x * 255.0) as u8,
            g: (vec.y * 255.0) as u8,
            b: (vec.z * 255.0) as u8,
        }
    }
}

/// Een witte pixel
pub const WHITE: Pixel = Pixel { r: 255, g: 255, b: 255 };

/// Een zwarte pixel
pub const BLACK: Pixel = Pixel { r:   0, g:   0, b:   0 };

/// Een grijze pixel
pub const GREY: Pixel = Pixel { r:  127, g: 127, b: 127 };
