use scene::Scene;
use pixel::Pixel;
use solid::RenderableSolid;
use pixel;
use light::light_ray::LightRay;
use hit::Hit;

#[allow(dead_code)]
pub fn handle_hit(ray: LightRay, _: &Scene, hit: Option<(Hit, &RenderableSolid)>) -> Pixel {
    match hit {
        Some((ref hit, _)) => { 
            let dist = hit.pos.dist(ray.ray.pos) as f64;
            let mut brightness = 10.0 / (dist * dist);
            if brightness > 1.0 { brightness = 1.0; }

            let pixel_brightness = (brightness * 255.0) as u8;

            Pixel::new(pixel_brightness, pixel_brightness, pixel_brightness)
        },
        None => {
            pixel::BLACK
        }
    }
}
