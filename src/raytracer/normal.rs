use scene::Scene;
use pixel::Pixel;
use pixel;
use light::light_ray::LightRay;
use solid::RenderableSolid;
use hit::Hit;
use vec::XYZ;

#[allow(dead_code)]
pub fn handle_hit(_: LightRay, _: &Scene, hit: Option<(Hit, &RenderableSolid)>) -> Pixel {
    match hit {
        Some((ref hit, _)) => { 
            let abs_normal = XYZ::new(hit.normal.x.abs(), hit.normal.y.abs(), hit.normal.z.abs()).normalize();
            Pixel::from_vector(abs_normal)
        },
        None => {
            pixel::BLACK
        }
    }
}
