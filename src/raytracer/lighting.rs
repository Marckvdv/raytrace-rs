use scene::Scene;
use ray::Ray;
use hit::Hit;
use light::light_ray::LightRay;
use light::Light;
use solid::RenderableSolid;
use raytracer::shadow_trace;
use raytracer::reflect;
use vec::VecFloat;

const NORMAL_FACTOR_CONSTANT: VecFloat = 1.0;
const REFLECTION_FACTOR_CONSTANT: VecFloat = 0.0;
const DISTANCE_FACTOR_CONSTANT: VecFloat = 1.0;

#[allow(dead_code)]
pub fn handle_hit(mut light_ray: LightRay, scene: &Scene, hit: Option<(Hit, &RenderableSolid)>) -> Light {
    match hit {
        Some((ref hit, ref hit_solid)) => { 
            for light in scene.lights.iter() {
                let to_light = Ray::new(hit.pos, (light.pos - hit.pos).normalize());
                if shadow_trace(&scene, hit.pos - light_ray.ray.dir * 0.000001, light.pos).is_none() {
                    let dist_squared = {
                        let towards = hit.pos - light.pos;
                        towards.dot(towards)
                    };

                    let distance_factor = 1.0 / dist_squared;
                    let distance_factor = 1.0 - (1.0 - distance_factor) * DISTANCE_FACTOR_CONSTANT;

                    let normal_factor = to_light.dir.dot(hit.normal).abs();  // Ensure that it's greater than zero.
                    let normal_factor = 1.0 - (1.0 - normal_factor) * NORMAL_FACTOR_CONSTANT;

                    let reflection_factor = to_light.dir.dot(reflect(light_ray.ray.dir, hit.normal)).abs();
                    let reflection_factor = 1.0 - (1.0 - reflection_factor) * REFLECTION_FACTOR_CONSTANT;

                    let factor = distance_factor * normal_factor * reflection_factor;
                    light_ray.light.add_light(&light.light, factor);
                }
            }

            light_ray.light.apply_material(&hit_solid.material);
        },
        None => {}
    }
    light_ray.light
}

