//! De code die door elke raytracer gebruikt wordt.

use std::sync::{Arc, Mutex};
use std::io;
use std::io::Write;

use vec::{XYZ, VecFloat};
use camera::Camera;
use scene::Scene;
use pixel::Pixel;
use solid::RenderableSolid;
use ray::Ray;
use light::light_ray::LightRay;
use light::Light;
use hit::Hit;
use thread_scoped;
use consts;

const PROGRESS_BAR_LENGTH: usize = 40;

/// Een simpele raytracer die alleen de afstand tot het geraakte voorwerp gebruikt. **Buiten
/// gebruik**
pub mod dist;

/// Een simpele raytracer die alleen gebruikt of het voorwerp geraakt is. **Buiten gebruik**
pub mod hit;

/// Een simpele raytracer die alleen de normaal van het geraakte voorwerp gebruikt. **Buiten
/// gebruik**
pub mod normal;

/// Een simpele raytracer die alleen bepaalt waar schaduw is. **Buiten gebruik**.
pub mod shadow;

/// Een raytracer die belichting doet aan de hand van golflengte's en de CIE kleurruimte.
pub mod lighting;

/// Bepaalt de refectie ray bij de gegeven inval en normaal vectoren.
#[allow(unused_variables, dead_code)]
pub fn reflect(ray: XYZ, normal: XYZ) -> XYZ {
    ray - ray.dot(normal) * 2.0
}

/// Bepaalt de hit die het dichtst bij is.
pub fn find_closest_hit(scene: &Scene, ray: Ray) -> Option<(Hit, &RenderableSolid)> {
    use std::mem;

    let mut closest_dist: VecFloat = consts::INFINITY;
    let mut closest_solid: &RenderableSolid = unsafe { mem::uninitialized() };
    let mut closest: Hit = unsafe { mem::uninitialized() };
    let mut hit = false;

    for renderable in scene.solids.iter() {
        match renderable.solid.intersect(&ray) {
            Some(ref hit_info) => {
                let dist = hit_info.pos.dist(ray.pos);
                if dist < closest_dist { 
                    closest_dist = dist;
                    closest_solid = &renderable;
                    closest = *hit_info;
                    hit = true;
                }
            },
            _ => (),
        }
    }

    if hit { Some((closest, closest_solid)) }
    else { None }
}

/// Tekent de scene door alle rays af (parallel) af te vuren.
pub fn render<F>(scene: &Scene, hit_func: &F, thread_count: usize) -> Vec<Pixel> 
        where F: Sync + Send + Fn(LightRay, &Scene, Option<(Hit, &RenderableSolid)>) -> Light {

    let res = scene.camera.get_res();
    let mut pixels = Vec::new();
    let mut threads = Vec::new();
    println!("Going to raytrace: {:?}", res);
    for _ in 0 .. PROGRESS_BAR_LENGTH {
        print!("_");
    }
    print!("\x1B[\0E");

    let pixel_count = res.0 * res.1;
    let pixels_per_thread = pixel_count / thread_count;
    let mut start_pixel = 0;
    let mut pixels_done = Arc::new(Mutex::new(0));

	for thread_number in 0 .. thread_count {
        let end_pixel = if thread_number == thread_count - 1 {
            pixel_count
        } else {
            start_pixel + pixels_per_thread
        };

        let mut pixels_done = pixels_done.clone();
        unsafe {
            threads.push(thread_scoped::scoped(move || {
                let mut thread_pixels = Vec::new();
                for i in start_pixel .. end_pixel {
                    let x = i % res.0;
                    let y = i / res.0;

                    let ray = Ray::new(scene.camera.get_pos(), scene.camera.get_dir((x, res.1 - y)));
                    let hit = find_closest_hit(scene, ray);
                    let light = hit_func(LightRay::new(Light::new(), ray), scene, hit);

                    thread_pixels.push(light.to_cie().to_rgb());

                    let mut pixels_done = pixels_done.lock().unwrap();
                    *pixels_done += 1;

                    print_progress(*pixels_done, pixel_count);
                }

                thread_pixels
            }));
        }

        start_pixel += pixels_per_thread;
    }

    for thread_handle in threads {
        pixels.extend_from_slice(&thread_handle.join());
    }
    println!("");

    pixels

}

pub fn print_progress(pixel: usize, pixels: usize) {
    if pixel % (pixels / PROGRESS_BAR_LENGTH) == 0 {
        print!("#");
        io::stdout().flush();
    }
}

/// Bepaalt of er vanaf het gegeven punt bij een lichtbron gekomen kan worden.
pub fn shadow_trace(scene: &Scene, hit_pos: XYZ, light_pos: XYZ) -> Option<(Hit, &RenderableSolid)> {
    let towards = light_pos - hit_pos;
    let full_dist = towards.length();
    let towards = towards / full_dist;

    let towards_ray = Ray::new(hit_pos, towards);

    let hit = find_closest_hit(scene, towards_ray);
    match hit {
        Some(ref hit_info) if hit_info.0.pos.dist(hit_pos) < full_dist => Some(*hit_info),
        _ => None
    }
}
