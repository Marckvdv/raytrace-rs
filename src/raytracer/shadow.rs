use pixel::Pixel;
use pixel;
use ray::Ray;
use raytracer;
use hit::Hit;
use scene::Scene;

#[allow(dead_code)]
fn handle_hit(_: Ray, scene: &Scene, hit: Option<Hit>) -> Pixel {
    match hit {
        Some(hit) => { 
            let shadow_hit = raytracer::shadow_trace(&scene, hit.pos, scene.lights[0].pos);
            match shadow_hit {
                Some(_) => pixel::GREY,
                None => pixel::WHITE,
            }
        },
        None => {
            pixel::BLACK
        }
    }
}
