use pixel::Pixel;
use pixel;
use ray::Ray;
use hit::Hit;
use scene::Scene;

#[allow(dead_code)]
pub fn handle_hit(_: Ray, _: &Scene, hit: Option<Hit>) -> Pixel {
    match hit {
        Some(_) => { pixel::WHITE },
        None => { pixel::BLACK }
    }
}
