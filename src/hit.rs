//! Het `Hit` type dat alle nodige informatie van een raakpunt bevat.
use vec::XYZ;

/// Het `Hit` type dat alle nodige informatie van een raakpunt bevat.
#[derive(Clone, Copy)]
pub struct Hit {
    /// De positie van het raakpunt
    pub pos: XYZ,

    /// De normaal van het raakpunt
    pub normal: XYZ,
}

impl Hit {
    /// Maakt een nieuw raakpunt aan, aan de hand van de gegeven positie en normaal.
    pub fn new(pos: XYZ, normal: XYZ) -> Hit {
        Hit {
            pos: pos,
            normal: normal,
        }
    }
}
